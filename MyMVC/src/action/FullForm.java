package action;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

import form.ActionForm;

public class FullForm {

	public static ActionForm full(String formType, HttpServletRequest request) {
		// TODO Auto-generated method stub
		ActionForm form = new ActionForm();
		try {
			Class clazz = Class.forName(formType);
			form = (ActionForm) clazz.newInstance();
			Field[] fields = clazz.getDeclaredFields();
			for (Field field : fields) {
				field.setAccessible(true);
				field.set(form, request.getParameter(field.getName()));
				field.setAccessible(false);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return form;
	}
	
}
