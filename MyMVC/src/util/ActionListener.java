package util;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ActionListener implements ServletContextListener{

	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		System.out.println("application destroy");
	}

	public void contextInitialized(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		ServletContext context = sce.getServletContext();
		String tomcatPath = context.getRealPath("\\");
		String configPath = context.getInitParameter("struts_config");
		Map<String, XmlBean> actionsMap = StrutsConfig.loadStrutsConfig(tomcatPath + configPath);
		context.setAttribute("actionsMap", actionsMap);
		System.out.println("initial load success");
	}
}
