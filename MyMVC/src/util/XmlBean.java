package util;

import java.util.Map;

public class XmlBean {
	
	private String formName;
	private String formType;
	private String actionType;
	private String actionPath;
	private Map<String,String> actionForwards;
	
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	public String getFormType() {
		return formType;
	}
	public void setFormType(String formType) {
		this.formType = formType;
	}
	public String getActionType() {
		return actionType;
	}
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
	public String getActionPath() {
		return actionPath;
	}
	public void setActionPath(String actionPath) {
		this.actionPath = actionPath;
	}
	public Map<String, String> getActionForwards() {
		return actionForwards;
	}
	public void setActionForwards(Map<String, String> actionForwards) {
		this.actionForwards = actionForwards;
	}
}
